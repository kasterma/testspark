sudo apt-get update
sudo apt-get install -y openjdk-6-jdk make tmux bash-completion

# to get emacs24 need to add ppa:cassou, for this need python-software-properties
sudo apt-get install -y python-software-properties
sudo add-apt-repository ppa:cassou/emacs
sudo apt-get update
sudo apt-get install -y emacs24

# get lein and link project files
sudo -u vagrant mkdir /home/vagrant/bin
sudo -u vagrant wget https://raw.github.com/technomancy/leiningen/stable/bin/lein
sudo -u vagrant chmod a+x lein
sudo -u vagrant mv lein /home/vagrant/bin/
sudo -u vagrant ln -s /vagrant/ /home/vagrant/project

sudo -u vagrant ln -fs /vagrant/vagrant/resources/dotfiles/dotprofile /home/vagrant/.profile
sudo -u vagrant ln -fs /vagrant/vagrant/resources/dotfiles/dotprofile /home/vagrant/.bashrc
sudo -u vagrant ln -fs /vagrant/vagrant/resources/dotfiles/dotemacs /home/vagrant/.emacs
sudo -u vagrant ln -fs /vagrant/vagrant/resources/dotfiles/dottmux.conf /home/vagrant/.tmux.conf
