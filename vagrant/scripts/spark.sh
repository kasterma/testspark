apt-get install -y git
wget http://d3kbcqa49mib13.cloudfront.net/spark-1.0.0.tgz
wget http://repo.typesafe.com/typesafe/ivy-releases/org.scala-sbt/sbt-launch/0.13.5/sbt-launch.jar
mv sbt-launch.jar bin
echo >> bin/sbt <<EOF
SBT_OPTS="-Xms512M -Xmx1536M -Xss1M -XX:+CMSClassUnloadingEnabled -XX:MaxPermSize=256M"
java $SBT_OPTS -jar `dirname $0`/sbt-launch.jar "$@"
EOF
chmod a+x bin/sbt
